# ------------------------
# All Imports
# ------------------------
import boto3
import requests
from requests.auth import HTTPBasicAuth
import json
import os
import traceback
import copy
import uuid
import sys
from awsglue.utils import getResolvedOptions
from datetime import datetime

# ------------------------
# Globals
# ------------------------
sapHostName = ""
sapPort = ""
sapUser = ""
sapPassword = ""
odpServiceName = ""
odpEntitySetName = ""
dataChunkSize = ""
metaDataDDBName = ""
dataS3Bucket = ""
dataS3Folder = ""
selfSignedCertificate = ""
selfSignedCertificateS3Bucket = ""
selfSignedCertificateS3Key = ""
reLoad = False
_athenacompatiblejson = False
_allowInValidCerts = False
sapClientId = ""
numberofrecs_ = ""
partition_key = ""

# ------------------------
# Get System Date and Timestamp for creating S3 partition
# ------------------------
def getDateTime():
    global partition_key
    curr_date = datetime.now()
    partition_key = datetime.strftime(curr_date, '%Y%m%d%H%M%S')
    #today = datetime.strftime(today, '%Y%m%d')
    #print ("Today's timestamp:", today_dir)
    #return partition_key

# ------------------------
# All Constants
# ------------------------
INITLOADING = "InitLoading"
INITLOADED = "InitLoaded"
DELTALOADING = "DeltaLoading"
DELTATOKEN = "!deltatoken="

# ------------------------
# Function to Set DDB after job completion
# ------------------------
def metadata_raw_prepared_pipeline(numberofrecs_, job_src):
    #partition_dir = getDateTime()
    client = boto3.resource('dynamodb')
    tbl = client.Table('metadata_raw_prepared_pipeline')
    input = {'partition_key': partition_key, 
                'State': 'RAW COMPLETED',
                'RawBucket': dataS3Bucket,
                'RawFolder': dataS3Folder,
                'RawJobName': 'AWS_GLUE_dh_xbr_raw_sap_odata_extractor',
                'RawEntryCount': numberofrecs_,
                'job_src': job_src
           }
    response = tbl.put_item(Item=input)
    print(response)
    return response
# ------------------------
# Function to Run Prepared Glue Job
# ------------------------
def run_prepared_job():    
    client = boto3.client('glue')
    response = client.start_job_run(
                   JobName = 'AkashTestJSONtoParquet',
                   Arguments = {
                     '--partition_key': partition_key,
                     '--dataS3Bucket': dataS3Bucket,
                     '--dataS3Folder': dataS3Folder,
                     '--ddb_metadatatable_name': 'metadata_raw_prepared_pipeline'} )
    return response

# ------------------------
# Initialize
# ------------------------
def _setResponse(success,message, data, numberofrecs):
    response = {
        'success'   : success,
        'message'   : message,
        'traceback' : traceback.format_exc(),
        'data'      : data,
        'numberofrecs' : numberofrecs
    }
    return response

    
dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
table = dynamodb.Table(metaDataDDBName)
response = _setResponse(False,"Error in fetching data from SAP. Check detailed logs",None,0)
print(table)

# ------------------------
# Main Extract entry point
# ------------------------
def extract():
    global response
    try:
        if reLoad == True:
            _extract(" ",True)
        else:
            print("inside calling _get_metadata()\n")
            metadata = _get_metadata()
            print("completed calling _get_metadata()\n")
            if metadata is None:
                _extract(" ",True)
            else:
                status = metadata.pop('status', " ")
                if status == INITLOADED or status == DELTALOADING:
                    _extract(metadata.pop('delta'," "),False)
                else:    
                    _extract(metadata.pop('next'," "),True)
    except Exception as e:
        response = _setResponse(False,str(e),None,0)

    return response    


# ------------------------
# Perform extract
# ------------------------
def _extract(link,isInit):
    print("inside _extract\n")
    global response
    url = link
    #partition_dir = getDateTime()
    if url == " ":
        #url = _get_base_url() + "/EntityOf" + odpEntitySetName + "?$format=json"
        url = _get_base_url() + "/" + odpEntitySetName + "?$format=json" + "&sap-client=" + sapClientId
        print(url)
        #https://WIDAPPECC01.sap.aldogroup.com:44300/sap/opu/odata/sap/ZAWS_PLANT_310820_SRV/?$format=xml&sap-client=100&sap-language=EN
    
    headers = {
        "prefer" : "odata.maxpagesize=" + dataChunkSize + ",odata.track-changes"
    }
    sapresponse =  _make_http_call_to_sap(url,headers)
    sapresponsebody = json.loads(sapresponse.text)
    _response = copy.deepcopy(sapresponsebody)

    d = sapresponsebody.pop('d',None)
    results = d.pop('results',None)
    for result in results:
        _metadata = result.pop('__metadata',None)
    
    if isInit == True:
        next = d.pop('__next'," ")
        if next == " ":
            _modify_ddb_table(INITLOADED,next,_get_delta_link())
        else:
            _modify_ddb_table(INITLOADING,next," ")
    else:
        delta = d.pop('__delta', " ")
        deltaTokenIndex = delta.find(DELTATOKEN)
        deltaLink = ""
        if deltaTokenIndex > -1:
            deltaToken = delta[deltaTokenIndex:len(delta)]
            deltaToken = deltaToken.replace(DELTATOKEN, "")
            deltaLink = _get_base_url() + "/DeltaLinksOf" + odpEntitySetName + "(" + deltaToken + ")/ChangesAfter?$format=json" + "&sap-client=" + sapClientId
        else:
            deltaLink = _get_delta_link()

        _modify_ddb_table(DELTALOADING," ",deltaLink)
    
    if len(results)<=0:
        response = _setResponse(True,"No data available to extract from SAP", _response, 0)
    elif(dataS3Bucket != ""):
        s3 = boto3.resource('s3')
        #dataS3Folder = dataS3Folder + '/' + partition_dir
        fileName = ''.join([dataS3Folder+'/'+partition_key,'/',str(uuid.uuid4().hex[:6]),odpServiceName, "_", odpEntitySetName,".json"]) 
        object = s3.Object(dataS3Bucket, fileName)
        if _athenacompatiblejson==True:
            object.put(Body=_athenaJson(results))
        else:    
            object.put(Body=json.dumps(results,indent=4))
            
        response = _setResponse(True,"Data successfully extracted and stored in S3 Bucket with key " + fileName, None, len(results))
    else:
        response = _setResponse(True,"Data successfully extracted from SAP", _response, len(results))

# ------------------------------------
# Conver JSON to athena format
# ------------------------------------
def _athenaJson(objects):
    return '\n'.join(json.dumps(obj) for obj in objects)

# ------------------------------------
# Get base url for HTTP calls to SAP
# ------------------------------------
def _get_base_url():
    global sapPort
    if sapPort == "":
        sapPort = "443"
    return "https://" + sapHostName + ":" + sapPort + "/sap/opu/odata/SAP/" + odpServiceName
    
# ------------------------------------
# Get the last available delta link
# ------------------------------------    
def _get_delta_link():
    try:
        url = _get_base_url() + "/DeltaLinksOf" + odpEntitySetName + "?$format=json" + "&sap-client=" + sapClientId
        sapresponse =  _make_http_call_to_sap(url,None)
        sapresponsebody = json.loads(sapresponse.text)
        d = sapresponsebody.pop('d',None)
        results = d.pop('results',None)
        if len(results) > 0:
            result = results[len(results)-1]
        
        changesAfter = result.pop('ChangesAfter',None)
        deferred = changesAfter.pop('__deferred',None)
        deltauri = deferred.pop('uri'," ")
        if deltauri != " ":
            deltauri = deltauri + "?$format=json" + "&sap-client=" + sapClientId
        return deltauri
    except:
        return " "
    
# ------------------------------------
# Call SAP HTTP endpoint
# ------------------------------------    
def _make_http_call_to_sap(url,headers):
    #global selfSignedCertificate
    certFileName = os.path.join('/tmp/','sapcert.crt')
    verify = True
    if selfSignedCertificate != "" :
        certfile = open(certFileName,'w')
        os.write(certfile,selfSignedCertificate)
        verify = certFileName
    elif selfSignedCertificateS3Bucket != "" :
        s3 = boto3.client('s3')
        verify = certFileName
        with open(certFileName, 'w') as f:
            s3.download_fileobj(selfSignedCertificateS3Bucket, selfSignedCertificateS3Key, f)
        certfile = open(certFileName,'r')
        print(certfile.read())
    elif _allowInValidCerts == True:
        verify = False
    return requests.get( url,auth=HTTPBasicAuth(sapUser,sapPassword), headers=headers,verify=verify)


# ------------------------------------
# Get metadata from DynamoDB
# ------------------------------------    
def _get_metadata():
    print("Inside _get_metadata()\n")
    print(table, "\n")
    ddbresponse = table.get_item(
        TableName=metaDataDDBName,
        Key={
            'odpServiceName': odpServiceName,
            'odpEntitySetName': odpEntitySetName
        }
    )
    if 'Item' in ddbresponse:
        print("return ", ddbresponse['Item'])
        return ddbresponse['Item']
    else:
        print ("Returning None from _get_metadata()")
        return None

# ------------------------------------
# Insert/Update records in DynamoDB
# ------------------------------------    
def _modify_ddb_table(status,next,delta):
    table.put_item(
        TableName=metaDataDDBName,
        Item={
                'odpServiceName': odpServiceName,
                'odpEntitySetName': odpEntitySetName,
                'status' : status,
                'next' : next,
                'delta' : delta,
        }
    )
#-- Call function----#
# ------------------------
# get Arguments
# ------------------------

print("Getting the arguments...\n")

args = getResolvedOptions(sys.argv, ['sapHostName','sapPort','odpServiceName','odpEntitySetName','dataChunkSize','metaDataDDBName','dataS3Bucket','sapAuthSecret', 'sapClientId', 'dataS3Folder', 'job_src'])

print("hostname is: ", args['sapHostName'])

# ------------------------
# execute program - consider creating a main method or move this to extract
# ------------------------

#def main():
    
sm = boto3.client('secretsmanager')
secretResponse = sm.get_secret_value(SecretId=args['sapAuthSecret'])
sapauth = json.loads(secretResponse['SecretString'])
sapHostName = args['sapHostName']
sapPort = args['sapPort']
sapUser = sapauth['user']
sapPassword = sapauth['password']
metaDataDDBName = args['metaDataDDBName']
odpServiceName = args['odpServiceName']
odpEntitySetName = args['odpEntitySetName']
dataChunkSize = args['dataChunkSize']
dataS3Bucket = args['dataS3Bucket']
dataS3Folder = args['dataS3Folder']
sapClientId = args['sapClientId']
job_src = args['job_src']
#VBELN = "0200000630"

_allowInValidCerts = True     ## Don't ever do this in production
_athenacompatiblejson = True


getDateTime()
extract()
metadata_raw_prepared_pipeline(response['numberofrecs'], job_src)
#run_prepared_job()
print(response)
print(response['numberofrecs'])
print("\nWriting Data at: s3://"+dataS3Bucket+"/"+dataS3Folder+"/"+partition_key+"/")
#if __name__ == "__main__":
#    main()

